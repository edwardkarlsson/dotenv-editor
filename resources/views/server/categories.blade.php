@foreach($categories as $category)
	<a
			href="/partial/category/{{ $category->id }}/keys"
			class="list-group-item js-load-keys"
			data-category-id="{{ $category->id }}"
	>
		<div class="row">
			<div class="col-xs-1">
				<i class="fa {{ $category->fa_class }}" aria-hidden="true"></i>
			</div>
			<div class="col-xs-10">
				{{ $category->name }}
			</div>
		</div>
	</a>
@endforeach

<a
		href="#"
		class="list-group-item"
		data-toggle="modal"
		data-target="#addCategoryModal"
>
	<div class="row">
		<div class="col-xs-1">
			<i class="fa fa-plus" aria-hidden="true"></i>
		</div>
		<div class="col-xs-10">
			<em>Add category</em>
		</div>
	</div>
</a>