@foreach($keys as $key)
	<a
			href="/partial/modal/key/{{ $key->id }}/edit"
			class="list-group-item js-load-value"
	        data-key-id="{{ $key->id }}"
	>
		<b>{{ $category->prefix }}_{{ strtoupper($key->key) }}</b>
		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
		@if($key->value)
			{{ $key->value }}
		@else
			<i>undefined</i>
		@endif
	</a>
@endforeach

<a href="/partial/modal/category/{{ $category->id }}/key/add" class="list-group-item" data-toggle="modal" data-target="#addKeyModal">
	<i class="fa fa-plus" aria-hidden="true"></i>
	<em>Add key</em>
</a>