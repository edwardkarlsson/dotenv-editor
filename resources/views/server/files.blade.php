@foreach($files as $file)
	<a
			href="/partial/file/{{ $file->id }}/categories"
			class="list-group-item js-load-category"
			data-file-id="{{ $file->id }}"
	>
		<div class="row">
			<div class="col-xs-1">
				<i class="fa fa-file-code-o" aria-hidden="true"></i>
			</div>
			<div class="col-xs-10">
				{{ $file->name }}
			</div>
		</div>
	</a>
@endforeach

<a href="#" class="list-group-item" data-toggle="modal" data-target="#addFileModal">
	<div class="row">
		<div class="col-xs-1">
			<i class="fa fa-plus" aria-hidden="true"></i>
		</div>
		<div class="col-xs-10">
			<em>Add file</em>
		</div>
	</div>
</a>


