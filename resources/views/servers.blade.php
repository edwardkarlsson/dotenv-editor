@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Servers</h3>
                </div>
                <ul class="list-group">
                    @foreach($servers as $server)
                        <a href="/server/{{ $server->name }}/{{ $server->id }}" class="list-group-item">
                            {{ $server->name }}
                        </a>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Groups</h3>
                </div>
                @foreach($groups as $name => $data)
                    <a href="/server-group/{{ $name }}" class="list-group-item">
                        <h4 class="list-group-item-heading">{{ $name }}</h4>
                        <p class="list-group-item-text">
                            <em>{{ implode(', ', $data) }}</em>
                        </p>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
