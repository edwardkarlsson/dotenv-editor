<li class="list-group-item text-right sub-menu js-key-menu">
	<a
			href="/partial/modal/key/{{ $key_id }}/edit"
			data-toggle="modal"
			data-tooltip="true"
			title="Edit key"
			data-target="#editKeyModal"
	><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	<a href="#" data-tooltip="true" title="Delete key"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
</li>