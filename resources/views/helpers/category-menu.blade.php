<li class="list-group-item text-right sub-menu js-category-menu">
	<a href="#" data-tooltip="true" title="Edit category"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	<a href="#" data-tooltip="true" title="Delete category"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
</li>