<li class="list-group-item sub-menu js-file-menu">
	<span class="pull-right">
		<a href="#" data-toggle="tooltip" title="Edit file"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
		<a href="#" data-toggle="tooltip" title="Duplicate file"><i class="fa fa-files-o" aria-hidden="true"></i></a>
		<a href="#" data-toggle="tooltip" title="Delete file"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
	</span>
	<a href="#" data-tooltip="true" title="Sync with server"><i class="fa fa-download" aria-hidden="true"></i></a>
	<a href="#" data-toggle="modal" data-target="#fileUploadModal" data-tooltip="true" title="Upload to server"><i class="fa fa-upload" aria-hidden="true"></i></a>
	<a href="#" data-tooltip="true" title="Show previous files"><i class="fa fa-history" aria-hidden="true"></i></a>
</li>