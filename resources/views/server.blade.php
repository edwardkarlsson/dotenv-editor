@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right js-refresh-files"><i class="fa fa-refresh" aria-hidden="true"></i></div>
                    <h3 class="panel-title">Files</h3>
                </div>
                <ul class="list-group" id="file-list">
                    @include('server.files')
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right js-refresh-categories"><i class="fa fa-refresh" aria-hidden="true"></i></div>
                    <h3 class="panel-title">Categories</h3>
                </div>
                <ul class="list-group" id="category-list">
                    <li class="list-group-item">Please select a file to work with.</li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="pull-right js-refresh-keys"><i class="fa fa-refresh" aria-hidden="true"></i></div>
                    <h3 class="panel-title">Keys</h3>
                </div>
                <ul class="list-group" id="key-list">
                    <li class="list-group-item">Please select a category to work with.</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addFileModal" tabindex="-1" role="dialog" aria-labelledby="addFileModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add file</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">File name</span>
                        <input type="hidden" id="server-id" value="{{ $server->id }}">
                        <input
                                id="file-name"
                                v-model="file_name_example"
                                type="text"
                                class="form-control"
                                placeholder="default"
                                aria-describedby="sizing-addon1"
                        >
                    </div>
                    <span v-if="file_name_example">
                        File name:
                        <code v-if="file_name_example !== 'default'">.env.<span>@{{ file_name_example }}</span></code>
                        <code v-if="file_name_example === 'default'">.env</code>
                        <br />
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-file-submit">Save file</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addCategoryModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add category</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Category name</span>
                        <input
                                id="category-name"
                                v-model="category_name_example"
                                type="text"
                                class="form-control"
                                placeholder="default"
                                aria-describedby="sizing-addon1"
                        >
                    </div>
                    <span v-if="category_name_example">
                        File output example:
                        <code># <span>@{{ category_name_example }}</span> category</code><br />
                    </span>
                    <br />
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Prefix</span>
                        <input
                                id="category-prefix"
                                v-model="category_prefix_example"
                                type="text"
                                class="form-control"
                                placeholder="DB"
                                aria-describedby="sizing-addon1"
                        >
                    </div>
                    <span v-if="category_prefix_example">
                        File output example: <code><span>@{{ category_prefix_example }}</span>_KEY=value</code><br />
                    </span>
                    <br />
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="sizing-addon1">Font Awesome Class</span>
                        <input
                                id="category-fa-class"
                                v-model="category_fa_class_example"
                                type="text"
                                class="form-control"
                                placeholder="fa-database"
                                aria-describedby="sizing-addon1"
                        >
                    </div>
                    <div v-if="category_fa_class_example">
                        File menu example:
                        <fa :class="category_fa_class_example"></fa>
                        @{{ category_name_example }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-category-submit">Save file</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addKeyModal" tabindex="-1" role="dialog" aria-labelledby="addKeyModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div class="modal fade" id="editKeyModal" tabindex="-1" role="dialog" aria-labelledby="editKeyModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>


    <div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="fileUploadModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>


    <input type="hidden" id="file-id">
    <input type="hidden" id="category-id">

    <div id="loadingSplash" style="display: none">
        <li class="list-group-item text-center">
            <i class="fa fa-lg fa-spin fa-cog" aria-hidden="true"></i>
        </li>
    </div>

    <div id="pendingFileSplash" style="display: none">
        <li class="list-group-item text-center">
             <em>Please select a file.</em>
        </li>
    </div>

    <div id="pendingCategorySplash" style="display: none">
        <li class="list-group-item text-center">
            <em>Please select a category.</em>
        </li>
    </div>
    
</div>
@endsection
