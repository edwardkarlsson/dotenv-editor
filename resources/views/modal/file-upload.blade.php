<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="addKeyModalLabel">Upload file <b>{{ $file->name }}</b></h4>
</div>
<div class="modal-body">
	<textarea
			id="file_content"
			class="form-control"
			style="height: 300px;"
	># File name: {{ $file->name }}
# Created at: {{ date('Y-m-d H:i:s') }}
# Created by: {{ $user->email }}

@foreach ($file->categories as $category)
# {{ $category->name }}
@foreach ($category->keys as $key)
{{ $category->prefix }}_{{ $key->key }}={{ $key->value }}
@endforeach

@endforeach
</textarea>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" id="file-upload-submit">Upload file</button>
</div>