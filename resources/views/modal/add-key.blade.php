<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="addKeyModalLabel">Add key</h4>
</div>
<div class="modal-body">
	<div class="input-group input-group-lg">
		<span class="input-group-addon" id="sizing-addon1"><i class="fa fa-key" aria-hidden="true"></i> {{ $category->prefix }}_</span>
		<input type="text" class="form-control" aria-describedby="sizing-addon1" id="key">
	</div>
	<br />
	<div class="input-group input-group-lg">
		<span class="input-group-addon" id="sizing-addon1"><i class="fa fa-dollar" aria-hidden="true"></i> Value</span>
		<input type="text" class="form-control" aria-describedby="sizing-addon1" id="value">
	</div>
	<br />
	<div class="input-group input-group-lg">
		<span class="input-group-addon" id="sizing-addon1">Description</span>
		<input type="text" class="form-control" placeholder="optional" aria-describedby="sizing-addon1" id="description">
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" id="add-key-submit">Save key</button>
</div>