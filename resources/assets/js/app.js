
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./config.js');
require('./server.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('fa', require('./components/FontAwesome.vue'));

const app = new Vue({
    el: '#app',
    data: {
        file_name_example: '',
        category_name_example: '',
        category_prefix_example: '',
        category_fa_class_example: '',
        message: 'here'
    }
});
