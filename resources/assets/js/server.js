$(function () {

	resetCategories();
	resetKeys();

	$(document).on("click", ".js-refresh-files", function () {
		reloadFiles();
		resetCategories();
		resetKeys();
	});

	$(document).on("click", ".js-refresh-categories", function () {
		reloadCategories();
		resetKeys();
	});

	$(document).on("click", ".js-refresh-keys", function () {
		reloadKeys();
	});

	$(document).on("click", "button#add-file-submit", function () {
		$.post("/ajax/add/server", {
			name: $('#file-name').val(),
			server_id: $('#server-id').val()
		})
			.done(function () {
				$('#addFileModal').modal('hide');
				reloadFiles();
				resetKeys();
			});
	});

	$(document).on("click", "button#add-category-submit", function () {
		$.post("/ajax/add/category", {
			name: $('#category-name').val(),
			prefix: $('#category-prefix').val(),
			fa_class: $('#category-fa-class').val(),
			file_id: $('#file-id').val()
		})
			.done(function () {
				$('#addCategoryModal').modal('hide');
				reloadCategories();
				resetKeys();
			});
	});

	$(document).on("click", "button#add-key-submit", function () {
		$.post("/ajax/add/key", {
			key: $('#key').val(),
			value: $('#value').val(),
			description: $('#description').val(),
			category_id: $('#category-id').val()
		})
			.done(function (data) {
				$('#addKeyModal').modal('hide');
				reloadKeys();
			});
	});

	$(document).on("click", "button#file-upload-submit", function () {
		$('#file-upload-submit').append(' <i class="fa fa-spin fa-refresh"></i>')
		$.post("/ajax/upload/file/" + $('#file-id').val(), {
			file_content: $('#file_content').val()
		})
			.done(function (data) {
				$('#fileUploadModal').modal('hide');
				alert('File has been uploaded.');
			});
	});

	$(document).on("click", ".js-load-category", function (e) {
		e.preventDefault();
		var clickedLink = $(this);
		deativateAllSiblings(clickedLink);
		clickedLink.addClass('active');
		$("#category-list").html('');
		$('input#file-id').val(clickedLink.data('file-id'));
		loadFileMenu(clickedLink);
		reloadCategories();
		resetKeys();
	});

	$(document).on("click", ".js-load-keys", function (e) {
		e.preventDefault();
		var clickedLink = $(this);
		deativateAllSiblings(clickedLink);
		clickedLink.addClass('active');
		$("#key-list").html('');
		$('input#category-id').val(clickedLink.data('category-id'));
		loadCategoryMenu(clickedLink);
		reloadKeys();
	});

	$(document).on("click", ".js-load-value", function(e) {
		e.preventDefault();
		var clickedLink = $(this);
		deativateAllSiblings(clickedLink);
		clickedLink.addClass('active');
		loadKeyMenu(clickedLink);
	});

	$('#addKeyModal').on('show.bs.modal', function () {
		$('#addKeyModal .modal-content').html('<div class="text-center"><br /><br /><i class="fa fa-lg fa-cog fa-spin" aria-hidden="true"></i><br /><br /><br /></div>');
		$.get("/partial/modal/category/" + $('#category-id').val() + "/key/add")
			.done(function (data) {
				$('#addKeyModal .modal-content').html(data).slideDown(200);
			});
	})


	$('#fileUploadModal').on('show.bs.modal', function () {
		$('#fileUploadModal .modal-content').html('<div class="text-center"><br /><br /><i class="fa fa-lg fa-cog fa-spin" aria-hidden="true"></i><br /><br /><br /></div>');
		$.get("/partial/modal/file/" + $('#file-id').val() + "/upload")
			.done(function (data) {
				$('#fileUploadModal .modal-content').html(data).slideDown(200);
			});
	})


});

function resetCategories() {
	$("#category-list").html($('div#pendingFileSplash').html());
}

function resetKeys() {
	$("#key-list").html($('div#pendingCategorySplash').html());
}


function reloadFiles() {
	var serverId = $('#server-id').val();
	$(".js-refresh-files").addClass('fa-spin');
	$("#file-list").html($('div#loadingSplash').html());
	$.get("/partial/server/" + serverId + "/files", function(data) {
		$("#file-list").html(data);
		$(".js-refresh-files").removeClass('fa-spin');
	});
}

function reloadCategories() {
	var fileId = $('#file-id').val();
	$(".js-refresh-categories").addClass('fa-spin');
	$("#category-list").html($('div#loadingSplash').html());
	$.get("/partial/file/" + fileId + "/categories", function(data) {
		$("#category-list").html(data);
		$(".js-refresh-categories").removeClass('fa-spin');
	});
}

function reloadKeys() {
	var categoryId = $('#category-id').val();
	var keyList = $("#key-list");
	$(".js-refresh-keys").addClass('fa-spin');
	keyList.html($('div#loadingSplash').html());
	$.get("/partial/category/" + categoryId + "/keys", function(data) {
		keyList.html(data);
		$(".js-refresh-keys").removeClass('fa-spin');
	});
}

function deativateAllSiblings(clickedLink) {
	clickedLink.parent().find('a').each(function () {
		$(this).removeClass('active');
	});
}

function loadFileMenu(clickedLink) {
	$('.js-file-menu').each(function () {
		$(this).slideUp("fast", function() { $(this).remove(); } );
	});
	$.get("/partial/filemenu/" + clickedLink.data('file-id'), function(data) {
		clickedLink.after(data);
		$('.js-file-menu').slideDown();
		$('[data-tooltip="true"]').tooltip()
	});
}

function loadCategoryMenu(clickedLink) {
	$('.js-category-menu').each(function () {
		$(this).slideUp("fast", function() { $(this).remove(); } );
	});
	$.get("/partial/categorymenu/" + clickedLink.data('category-id'), function(data) {
		clickedLink.after(data);
		$('.js-category-menu').slideDown();
		$('[data-tooltip="true"]').tooltip()
	});
}

function loadKeyMenu(clickedLink) {
	$('.js-key-menu').each(function () {
		$(this).slideUp("fast", function() { $(this).remove(); } );
	});
	$.get("/partial/keymenu/" + clickedLink.data('key-id'), function(data) {
		clickedLink.after(data);
		$('.js-key-menu').slideDown();
		$('[data-tooltip="true"]').tooltip()
	});
}