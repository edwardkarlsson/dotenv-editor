<?php

namespace App\Http\Controllers;

use App\Server;
use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        foreach(config('remote.connections') as $server => $data) {
            $servers[] = Server::firstOrCreate(['name' => $server]);
        }

        $servers = collect($servers);


        return view('servers')
            ->with('servers', $servers)
            ->with('groups', config('remote.groups'));
    }

}
