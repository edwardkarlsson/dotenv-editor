<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\AddKeyRequest;
use App\Http\Requests\AddServerRequest;
use App\Key;
use App\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use SSH;

class ServerController extends Controller
{
    public function index(Request $request)
    {
        $server = Server::where('name', $request->servername)
            ->where('id', $request->serverid)
            ->first();

        $files = File::where('server_id', $server->id)->get();

        return view('server')
            ->with('server', $server)
            ->with('files', $files);
    }


    public function addServer(AddServerRequest $request)
    {
        $file = File::create(['name' => $request->name, 'server_id' => $request->server_id]);
        $this->createDefaultData($file->id);
        return $file;
    }

    public function addCategory(AddCategoryRequest $request)
    {
        return Category::create([
            'name' => $request->name,
            'prefix' => $request->prefix,
            'fa_class' => $request->fa_class,
            'file_id' => $request->file_id,
        ]);
    }

    public function addKey(AddKeyRequest $request)
    {
        return Key::create([
            'key' => $request->key,
            'value' => $request->value,
            'description' => $request->description,
            'category_id' => $request->category_id
        ]);
    }


    public function modalEditKey(Request $request)
    {
        $key = Key::find($request->keyid);
        dd($key->category);
        return view('modal.edit-key')
            ->with('key', $key);
    }

    public function modalAddKey(Request $request)
    {
        $category = Category::find($request->category_id);
        return view('modal.add-key')
            ->with('category', $category);
    }

    public function modalUploadFile(Request $request)
    {
        $file = File::find($request->file_id);
        $user = Auth::user();
        return view('modal.file-upload')
            ->with('file', $file)
            ->with('user', $user);
    }

    public function uploadFile(Request $request)
    {
        $file = File::find($request->file_id);
        $remotePath = config('remote.connections.'.$file->server->name.'.path') . $file->name;
        $localPath = $file->server->name . '/' . date('Ymd_His') . '/' . $file->name;
        Storage::disk('local')->put($localPath, $request->file_content);
        SSH::into($file->server->name)->put(storage_path('app/' . $localPath), $remotePath);
    }

    public function showFiles(Request $request)
    {
        $server = Server::where('id', $request->serverid)->first();
        $files = File::where('server_id', $server->id)->get();
        return view('server.files')
            ->with('files', $files);
    }

    public function showCategories(Request $request)
    {
        $categories = Category::where('file_id', $request->fileid)->get();
        return view('server.categories')
            ->with('categories', $categories)
            ->with('file_id', $request->fileid);
    }

    public function showKeys(Request $request)
    {
        $category = Category::find($request->categoryid);

        return view('server.keys')
            ->with('category', $category)
            ->with('keys', $category->keys);
    }

    public function showFileMenu(Request $request)
    {
        return view('helpers.file-menu');
    }

    public function showCategoryMenu(Request $request)
    {
        return view('helpers.category-menu');
    }

    public function showKeyMenu(Request $request)
    {
        return view('helpers.key-menu')
            ->with('key_id', $request->keyid);
    }

    private function createDefaultData($fileId)
    {
        Category::create(['name' => 'Database', 'prefix' => 'DB', 'fa_class' => 'fa-database', 'file_id' => $fileId]);
        Category::create(['name' => 'Email IDs', 'prefix' => 'EMAIL', 'fa_class' => 'fa-envelope-o', 'file_id' => $fileId]);
    }
}
