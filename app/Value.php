<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    public function key()
    {
        return $this->belongsTo('App\Key');
    }
}
