<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'server_id'];

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function server()
    {
        return $this->belongsTo('App\Server');
    }

    public function getNameAttribute($value)
    {
        if ($value === 'default') {
            return '.env';
        } else {
            return '.env.' . $value;
        }
    }
}
