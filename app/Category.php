<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'prefix', 'fa_class', 'file_id'];

    public function keys()
    {
        return $this->hasMany('App\Key');
    }

    public function file()
    {
        return $this->belongsTo('App\File');
    }

}
