<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    protected $fillable = ['key', 'value', 'description', 'category_id'];


    public function value()
    {
        return $this->hasOne('App\Value');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
