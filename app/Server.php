<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = ['name'];

    public function files()
    {
        return $this->hasMany('App\Files');
    }

}
