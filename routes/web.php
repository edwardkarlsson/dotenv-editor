<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'TestController@test');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/server/{servername}/{serverid}', 'ServerController@index');

Route::post('/ajax/add/server', 'ServerController@addServer');
Route::post('/ajax/add/category', 'ServerController@addCategory');
Route::post('/ajax/add/key', 'ServerController@addKey');
Route::post('/ajax/upload/file/{file_id}', 'ServerController@uploadFile');

Route::get('/partial/server/{serverid}/files', 'ServerController@showFiles');
Route::get('/partial/file/{fileid}/categories', 'ServerController@showCategories');
Route::get('/partial/category/{categoryid}/keys', 'ServerController@showKeys');

Route::get('/partial/modal/category/{category_id}/key/add', 'ServerController@modalAddKey');
Route::get('/partial/modal/key/{keyid}/edit', 'ServerController@modalEditKey');
Route::get('/partial/modal/file/{file_id}/upload', 'ServerController@modalUploadFile');

Route::get('/partial/filemenu/{fileid}', 'ServerController@showFileMenu');
Route::get('/partial/categorymenu/{categoryid}', 'ServerController@showCategoryMenu');
Route::get('/partial/keymenu/{keyid}', 'ServerController@showKeyMenu');